#ifndef FACTORIAL_H
#define FACTORIAL_H


class Factorial
{
private:
    int n_;
    int factorial_;
public:
    Factorial();
    void setN(int);
    int getN();
    void setFactorial(int);
    void calDisplay();
    void run();

};

#endif // FACTORIAL_H
